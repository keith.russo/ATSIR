/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package structs

// LogLevel - The level of logging to allow in the console
type LogLevel int

const (
	// Trace - Designates finer-grained informational events than the DEBUG.
	Trace = iota

	// Debug - Designates fine-grained informational events that are most useful to debug an application.
	Debug

	// Info - Designates informational messages that highlight the progress of the application at coarse-grained level.
	Info

	// Warning - Designates potentially harmful situations.
	Warning

	// Error - Designates error events that might still allow the application to continue running.
	Error

	// Fatal - Designates very severe error events that will presumably lead the application to abort.
	Fatal
)

// Settings - The structure of the data/bot.settings file
type Settings struct {
	DiscordToken    string   `json:"discord_token"`
	DiscordClientID string   `json:"discord_client_id"`
	Language        string   `json:"language"`
	BotName         string   `json:"bot_name"`
	Prefix          string   `json:"prefix"`
	IgnoreBots      bool     `json:"ignore_bots"`
	LogLevel        LogLevel `json:"log_level"`
}
