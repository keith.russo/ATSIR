PROJECT_NAME := "ATSIR"
PKG := "gitlab.com/aguemort/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build_windows build_mac build_deb build_pi

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

dep: ## Get the dependencies
	@echo "fetching Go dependencies..."
	@go get -d ./...

build_windows: dep ## Build the binary file
	@echo "Creating Windows binaries..."
	@env GOOS=windows GOARCH=amd64 go build -o ./atsir_windows_amd64.exe $(PKG)
	@env GOOS=windows GOARCH=386 go build -o ./atsir_windows_386.exe $(PKG)

build_mac: dep ## Build the binary file
	@echo "Creating Mac binaries..."
	@env GOOS=darwin GOARCH=amd64 go build -o ./atsir_mac_amd64.dmg $(PKG)

build_deb: dep ## Build the binary file
	@echo "Creating Debian binaries..."
	@env GOOS=linux GOARCH=amd64 go build -o ./atsir_linux_amd64 $(PKG)
	@env GOOS=linux GOARCH=386 go build -o ./atsir_linux_386 $(PKG)

build_pi: dep ## Build the binary file
	@echo "Creating Raspberry Pi binaries..."
	@env GOOS=linux GOARCH=arm GOARM=7 go build -o ./atsir_pi_arm7 $(PKG)
	@env GOOS=linux GOARCH=arm GOARM=6 go build -o ./atsir_pi_arm6 $(PKG)

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'