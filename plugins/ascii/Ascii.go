/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package ascii

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"strings"

	"github.com/common-nighthawk/go-figure"
	"gitlab.com/aguemort/ATSIR/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

type asciiSettingsStruct struct {
	Font string
}

var asciiSettings = &asciiSettingsStruct{}

func init() {
	if _, err := os.Stat("data"); os.IsNotExist(err) {
		_ = os.Mkdir("data", os.ModeDir)
	}

	if _, err := os.Stat("data/ascii.settings"); os.IsNotExist(err) {
		utilities.Info("Ascii settings file not found.  Creating it...")
		asciiSettings := &asciiSettingsStruct{
			Font: "lockergnome",
		}

		file, _ := json.MarshalIndent(asciiSettings, "", "  ")
		_ = ioutil.WriteFile("data/ascii.settings", file, 0644)
	}

	settingJSON, _ := utilities.ReadJSONFile("data/ascii.settings")
	_ = json.Unmarshal(settingJSON, asciiSettings)
}

// Art - The primary handler for the Art command
func Art(ch *structs.Command, args []string) error {
	success := utilities.SendCodeBlockMessage(
		ch,
		ch.Message.ChannelID,
		figure.NewFigure(strings.Join(args, " "), asciiSettings.Font, true).String(), args[0])

	if !success {
		go utilities.SendPlainTextMessage(
			ch,
			ch.Message.ChannelID,
			"Your Word Art exceeds Discord's maximum allowed characters. "+
				"Try again with a shorter word or a different style art.")

		utilities.Error("ASCII message exceeded Discord character limits.", len(figure.NewFigure(args[0], asciiSettings.Font, true).String()))
	}
	return nil
}

// SetFont - The primary handler for the Art SetFont sub-command
func SetFont(ch *structs.Command, args []string) error {
	if len(args) == 0 {
		utilities.Warning("No font name found.")
		return errors.New("no font name specified")
	}

	asciiSettings.Font = strings.ToLower(args[0])

	file, _ := json.MarshalIndent(asciiSettings, "", "  ")
	_ = ioutil.WriteFile("data/ascii.settings", file, 0644)

	return nil
}
