/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package starboard

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/aguemort/ATSIR/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

type starboardSettingsStruct struct {
	Channel string `json:"channel,omitempty"`
	Emote   string `json:"emote,omitempty"`
}

type starboardTrackerInternal struct {
	GuildID           string `json:"guild_id"`
	OriginalChannelID string `json:"original_channel_id"`
	OriginalMessageID string `json:"original_message_id"`
	Emoji             string `json:"emoji"`
	Count             int    `json:"count"`
	BoardChannelID    string `json:"board_channel_id"`
	BoardMessageID    string `json:"board_message_id"`
}

type starboardTracker struct {
	Items []*starboardTrackerInternal
}

var settings = &starboardSettingsStruct{}
var tracker = &starboardTracker{}

func init() {
	if _, err := os.Stat("data/starboard.settings"); os.IsNotExist(err) {
		utilities.Info("StarBoard settings file not found.  Creating it...")
		settings.Channel = ""
		settings.Emote = "⭐"

		file, _ := json.MarshalIndent(settings, "", "  ")
		_ = ioutil.WriteFile("data/starboard.settings", file, 0644)
	}

	settingsJSON, _ := utilities.ReadJSONFile("data/starboard.settings")
	_ = json.Unmarshal(settingsJSON, settings)

	if _, err := os.Stat("data/starboard.tracker"); os.IsNotExist(err) {
		utilities.Info("StarBoard tracker file not found.  Creating it...")

		file, _ := json.MarshalIndent(tracker, "", "  ")
		_ = ioutil.WriteFile("data/starboard.tracker", file, 0644)
	}

	trackerJSON, _ := utilities.ReadJSONFile("data/starboard.tracker")
	_ = json.Unmarshal(trackerJSON, tracker)
}

// UpdateBoardChannel - Adds or updates the starboard channel
func UpdateBoardChannel(ch *structs.Command, args []string) error {
	utilities.Debug(args)
	if len(args) == 0 {
		return errors.New("no channel mentioned")
	}

	channel := strings.ReplaceAll(args[0], "<", "")
	channel = strings.ReplaceAll(channel, ">", "")
	channel = strings.ReplaceAll(channel, "#", "")

	settings.Channel = channel
	// Write the channel to file
	file, _ := json.MarshalIndent(settings, "", "  ")
	_ = ioutil.WriteFile("data/starboard.settings", file, 0644)

	return nil
}

// OnMessageReactionAdd - Primary handler for the MessageReactionAdd event
func OnMessageReactionAdd(session *discordgo.Session, reaction *discordgo.MessageReactionAdd) {
	if reaction.UserID == utilities.Settings.DiscordClientID ||
		(settings.Channel == "" || settings.Emote == "") ||
		settings.Channel == reaction.ChannelID ||
		settings.Emote != reaction.Emoji.Name {
		return
	}

	utilities.Trace("All checks OK")

	embed, channel := buildEmbed(session, reaction)

	// If another user added a star, just increment the emoji count
	// We don't want duplicate entries
	if checkTracked(reaction) {
		for index, tracked := range tracker.Items {
			if tracked.OriginalMessageID == reaction.MessageID {
				tracker.Items[index].Count++
				writeToTracker()
			}
		}
	} else { // Otherwise, we send the message to the starboard
		if sentMsg := utilities.SendEmbedMessageElsewhere(session, channel.ID, embed); sentMsg != nil {
			newTracker := &starboardTrackerInternal{
				GuildID:           reaction.GuildID,
				OriginalChannelID: reaction.ChannelID,
				OriginalMessageID: reaction.MessageID,
				Emoji:             reaction.Emoji.Name,
				Count:             1,
				BoardChannelID:    settings.Channel,
				BoardMessageID:    sentMsg.ID,
			}
			tracker.Items = append(tracker.Items, newTracker)
			writeToTracker()
		}
	}
}

func writeToTracker() {
	file, _ := json.MarshalIndent(tracker, "", "  ")
	err := ioutil.WriteFile("data/starboard.tracker", file, 0644)
	if err != nil {
		utilities.Error(err)
	}
}

func checkTracked(reaction *discordgo.MessageReactionAdd) bool {
	for _, tracked := range tracker.Items {
		if tracked.OriginalMessageID == reaction.MessageID {
			return true
		}
	}

	return false
}

// OnMessageReactionRemove - Primary handler for the MessageReactionRemove event
func OnMessageReactionRemove(session *discordgo.Session, reaction *discordgo.MessageReactionRemove) {
	for index, tracked := range tracker.Items {
		// Need to make sure we have the correct star and the correct message ONLY!
		if reaction.Emoji.Name == tracked.Emoji && reaction.MessageID == tracked.OriginalMessageID {
			tracked.Count = tracked.Count - 1
			if tracked.Count <= 0 {
				// If there are no more stars, then we remove it from the tracker and delete the msg from the starboard
				tracker.Items = append(tracker.Items[:index], tracker.Items[index+1:]...)
				_ = session.ChannelMessageDelete(tracked.BoardChannelID, tracked.BoardMessageID)
			}
			writeToTracker()
		}
	}
}

// OnMessageReactionRemoveAll - Primary handler for the MessageReactionRemoveAll event
func OnMessageReactionRemoveAll(session *discordgo.Session, reaction *discordgo.MessageReactionRemoveAll) {
	for index, tracked := range tracker.Items {
		if reaction.Emoji.Name == tracked.Emoji && reaction.MessageID == tracked.OriginalMessageID {
			tracker.Items = append(tracker.Items[:index], tracker.Items[index+1:]...)
			_ = session.ChannelMessageDelete(tracked.BoardChannelID, tracked.BoardMessageID)
			writeToTracker()
		}
	}
}

func buildEmbed(session *discordgo.Session, reaction *discordgo.MessageReactionAdd) (*discordgo.MessageEmbed, *discordgo.Channel) {
	msg, err := session.ChannelMessage(reaction.ChannelID, reaction.MessageID)
	channel, _ := session.Channel(settings.Channel)
	ogChannel, _ := session.Channel(reaction.ChannelID)
	if err != nil {
		utilities.Warning("Unable to read the original message", err)
		return nil, nil
	}

	var embed *discordgo.MessageEmbed

	if len(msg.Embeds) > 0 {
		embed = msg.Embeds[0]
	} else if len(msg.Embeds) == 0 {
		embed = &discordgo.MessageEmbed{
			Type:        "rich",
			Title:       "New starred post from " + msg.Author.Username,
			Description: msg.Content,
			Timestamp:   time.Now().Format(time.RFC3339),
			Color:       123456,
			Footer: &discordgo.MessageEmbedFooter{
				Text:    "Originally posted in #" + ogChannel.Name,
				IconURL: msg.Author.AvatarURL(""),
			},
		}
	}

	return embed, channel
}
