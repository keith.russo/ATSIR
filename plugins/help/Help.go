/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package help

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/aguemort/ATSIR/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

// Help - Primary handler for the help command
func Help(ch *structs.Command, args []string) error {
	embed := &discordgo.MessageEmbed{
		Type:        "rich",
		Title:       utilities.Settings.BotName + " Command List",
		Description: "Please note that each command has its own specific help text.  Access it by calling `[command] help`",
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:   utilities.Settings.Prefix + "art",
				Value:  "Prints ASCII art for the specified word",
				Inline: false,
			},
			{
				Name:   utilities.Settings.Prefix + "cat",
				Value:  "Displays a random cat image with some basic information about it (if provided)",
				Inline: false,
			},
			{
				Name:   utilities.Settings.Prefix + "dog",
				Value:  "Displays a random dog image with some info about it (if provided)",
				Inline: false,
			},
			{
				Name:   utilities.Settings.Prefix + "help",
				Value:  "Displays the bot's command list",
				Inline: false,
			},
			{
				Name:   utilities.Settings.Prefix + "invite",
				Value:  "Displays the bot's invite link",
				Inline: false,
			},
			{
				Name:   utilities.Settings.Prefix + "movie",
				Value:  "Searches for information related to the specified movie",
				Inline: false,
			},
			{
				Name:   utilities.Settings.Prefix + "twitch",
				Value:  "Query the Twitch REST API for the specified info. The base command has no effect",
				Inline: false,
			},
		},
		Footer: &discordgo.MessageEmbedFooter{
			Text: "ATSIR - Almighty Tallest Supreme Invasion Robot\n" +
				"All code written by Keith Russo <keith.russo@veteransoftare.us>\n" +
				"Logo design and implementation by: MikeyTopCat <https://twitch.tv/mikeytopcat>\n",
			IconURL: "https://cdn.discordapp.com/attachments/230176740641603584/654059369318711336/atsir.png",
		},
	}
	utilities.SendEmbedMessage(ch, "", embed)

	return nil
}
