/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package weather

import (
	json2 "encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	weather "gitlab.com/aguemort/ATSIR/plugins/weather/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

var (
	// AppID - The API key for using OpenWeatherApp API
	AppID = "d75e2ffcebb386e194161b7ec8854653"
	uri   = "https://api.openweathermap.org/data/2.5/weather"
)

// GetCurrentWeather - Primary handler for the upcoming weather search functionality
func GetCurrentWeather(city []string) (*weather.CurrentWeather, error) {

	citiesRaw, err := utilities.ReadJSONFile("cities.weather")
	if err != nil {
		utilities.Error("Error reading weather cities file...", err)
	}

	cities := &weather.Cities{}

	err = json2.Unmarshal(citiesRaw, &cities)
	if err != nil {
		utilities.Error("Error unmarshalling weather city list")
		return nil, nil
	}

	for _, cityName := range cities.City {
		if strings.ToLower(city[0]) == strings.ToLower(cityName.Name) {
			return GetCurrentWeatherByCityID(cityName.ID)
		}
	}
	return nil, errors.New("unknown city")
}

// GetCurrentWeatherByCityName - The handler for searching by city name
func GetCurrentWeatherByCityName(cityName string) (*weather.CurrentWeather, error) {
	currentData := &weather.CurrentWeather{}

	response, err := http.Get(uri + "?q=" + cityName + "&APPID=" + AppID)
	if err != nil {
		utilities.Error("Error fetching weather data", err)
		return nil, err
	}

	raw, err := ioutil.ReadAll(response.Body)
	if err != nil {
		utilities.Error("Error reading weather data response", err)
		return nil, err
	}

	err = json2.Unmarshal(raw, &currentData)
	if err != nil {
		utilities.Error("Error unmarshalling weather response JSON", err)
		return nil, err
	}

	return currentData, nil
}

// GetCurrentWeatherByCityID - The handler for searching for a city by ID
func GetCurrentWeatherByCityID(cityID int) (*weather.CurrentWeather, error) {
	currentData := &weather.CurrentWeather{}

	response, err := http.Get(uri + "?q=" + strconv.Itoa(cityID) + "&APPID=" + AppID)
	if err != nil {
		utilities.Error("Error fetching weather data", err)
		return nil, err
	}

	raw, err := ioutil.ReadAll(response.Body)
	if err != nil {
		utilities.Error("Error reading the response body for weather data", err)
		return nil, err
	}

	err = json2.Unmarshal(raw, &currentData)
	if err != nil {
		utilities.Error("Error unmarshalling the response body JSON", err)
		return nil, err
	}

	return currentData, nil
}
