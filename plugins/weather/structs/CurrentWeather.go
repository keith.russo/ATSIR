/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package weather

// CurrentWeather - Struct for the Current weather API
// Reference: https://openweathermap.org/current#current_JSON
type CurrentWeather struct {
	Coordinates struct {
		Longitude float32 `json:"lon"` // City geo location, longitude
		Latitude  float32 `json:"lat"` // City geo location, latitude
	} `json:"coord"`
	Weather []struct { // (more info Weather condition codes)
		ID          int32  `json:"id"`          // Weather condition id
		Main        string `json:"main"`        // Group of weather parameters (Rain, Snow, Extreme etc.)
		Description string `json:"description"` // Weather condition within the group
		Icon        string `json:"icon"`        // Weather icon id
	} `json:"weather"`
	Base string `json:"base"` // Internal parameter
	Main struct {
		Temperature    float32 `json:"temp"`                 // Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
		Pressure       int32   `json:"pressure"`             // Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
		Humidity       uint32  `json:"humidity"`             // Humidity, %
		TemperatureMin float32 `json:"temp_min"`             // Minimum temperature at the moment. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
		TemperatureMax float32 `json:"temp_max"`             // Maximum temperature at the moment. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
		SeaLevel       uint32  `json:"sea_level,omitempty"`  // Atmospheric pressure on the sea level, hPa
		GroundLevel    uint32  `json:"grnd_level,omitempty"` // Atmospheric pressure on the ground level, hPa
	} `json:"main"`
	Visibility uint32 `json:"visibility"`
	Wind       struct {
		Speed   float32 `json:"speed"` // Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
		Degrees uint32  `json:"deg"`   // Wind direction, degrees (meteorological)
	} `json:"wind"`
	Clouds struct {
		All uint32 `json:"all,omitempty"` // Cloudiness, %
	} `json:"clouds,omitempty"`
	Rain struct {
		OneHour   uint32 `json:"1h,omitempty"` // Rain volume for the last 1 hour, mm
		ThreeHour uint32 `json:"3h,omitempty"` // Rain volume for the last 3 hours, mm
	} `json:"rain,omitempty"`
	Snow struct {
		OneHour   uint32 `json:"1h,omitempty"` // Snow volume for the last 1 hour, mm
		ThreeHour uint32 `json:"3h,omitempty"` // Snow volume for the last 3 hours, mm
	} `json:"snow,omitempty"`
	DateTime uint64 `json:"dt,omitempty"` // Time of data calculation, unix, UTC
	System   struct {
		Type    int8    `json:"type"`    // Internal parameter
		ID      int16   `json:"id"`      // Internal parameter
		Message float32 `json:"message"` // Internal parameter
		Country string  `json:"country"` // Country code (GB, JP etc.)
		Sunrise int64   `json:"sunrise"` // Sunrise time, unix, UTC
		Sunset  int64   `json:"sunset"`  // Sunset time, unix, UTC
	} `json:"sys"`
	Timezone int32  `json:"timezone,omitempty"` // Shift in seconds from UTC
	ID       uint32 `json:"id"`                 // City ID
	Name     string `json:"name"`               // City name
	Cod      int32  `json:"cod"`                // Internal parameter
}
