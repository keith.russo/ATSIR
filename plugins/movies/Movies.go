/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package movies

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/gosimple/slug"
	"gitlab.com/aguemort/ATSIR/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

type movieData struct {
	Title       string `json:"Title"`
	Year        string `json:"Year"`
	Rating      string `json:"Rated"`
	ReleaseDate string `json:"Released"`
	Runtime     string `json:"Runtime"`
	Genre       string `json:"Genre"`
	Director    string `json:"Director"`
	Writer      string `json:"Writer"`
	Actors      string `json:"Actors"`
	Plot        string `json:"Plot"`
	Language    string `json:"Language"`
	Country     string `json:"Country"`
	Awards      string `json:"Awards"`
	Poster      string `json:"Poster"`
	Ratings     []struct {
		Source string `json:"Source"`
		Value  string `json:"Value"`
	} `json:"Ratings"`
	Metascore  string `json:"Metascore"`
	ImdbRating string `json:"imdbRating"`
	ImdbVotes  string `json:"imdbVotes"`
	ImdbID     string `json:"imdbID"`
	Type       string `json:"Type"`
	DvdRelease string `json:"DVD"`
	BoxOffice  string `json:"BoxOffice"`
	Production string `json:"Production"`
	Website    string `json:"Website"`
	Response   string `json:"Response"`
}

type movieSettingsStruct struct {
	APIKey string `json:"api_key"`
	Color  struct {
		Dec int    `json:"dec"`
		Hex string `json:"hex"`
	} `json:"color"`
}

var movieSettings = &movieSettingsStruct{}

func init() {
	if _, err := os.Stat("data/movie.settings"); os.IsNotExist(err) {
		utilities.Info("Movies settings file not found.  Creating it...")
		movieSettings.APIKey = "e5d6a1ba"
		movieSettings.Color.Dec = 16106776
		movieSettings.Color.Hex = "F5C518"

		file, _ := json.MarshalIndent(movieSettings, "", "  ")
		_ = ioutil.WriteFile("data/movie.settings", file, 0644)
	}

	settingsJSON, _ := utilities.ReadJSONFile("data/movie.settings")
	_ = json.Unmarshal(settingsJSON, movieSettings)
}

// GetMovieData - The primary handler for the Movie command
func GetMovieData(ch *structs.Command, args []string) error {
	utilities.Debug(args)
	if len(args) == 0 {
		// TODO: add error response here
		return nil
	}

	client := http.DefaultClient
	request, err := http.NewRequest("GET", "https://www.omdbapi.com/", nil)
	if request == nil || err != nil {
		utilities.Warning("Error setting up API request", err)
		return err
	}
	request.Header.Set("Accept", "application/json")

	query := request.URL.Query()
	query.Add("apikey", movieSettings.APIKey)
	query.Add("t", strings.Join(args, " "))
	query.Add("type", "movie")
	request.URL.RawQuery = query.Encode()

	utilities.Trace(request.URL.String())

	response, err := client.Do(request)
	if err != nil {
		utilities.Warning("Error receiving response from OMDB")
		return err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {

		}
	}(response.Body)

	if response == nil {
		utilities.Warning("Error receiving a response from OMDB", err)
		return err
	}

	if response.StatusCode == 404 {
		utilities.Warning("Movie not found", response)
		return err
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		utilities.Warning("Error reading response body", err)
		return err
	}

	movie := &movieData{}
	err = json.Unmarshal(body, &movie)
	if err != nil {
		utilities.Warning("Error unmarshalling the LMDB response body", err)
		return err
	}

	go utilities.SendEmbedMessage(ch, "", buildMovieEmbed(ch, movie, movieSettings))

	return nil
}

func buildMovieEmbed(ch *structs.Command, data *movieData, settings *movieSettingsStruct) *discordgo.MessageEmbed {
	m := data

	var ratings string

	for i := 0; i < len(m.Ratings); i++ {
		if m.Ratings[i].Source == "Internet Movie Database" {
			ratings += "[" + m.Ratings[i].Source + "](https://imdb.com/title/" + m.ImdbID + "): " +
				m.Ratings[i].Value + "\n"
		} else if m.Ratings[i].Source == "Rotten Tomatoes" {
			ratings += "[" + m.Ratings[i].Source + "](https://rottentomatoes.com/m/" + slug.Make(m.Title) + "): " +
				m.Ratings[i].Value + "\n"
		} else if m.Ratings[i].Source == "Metacritic" {
			ratings += "[" + m.Ratings[i].Source + "](https://www.metacritic.com/movie/" + slug.Make(m.Title) + "): " +
				m.Ratings[i].Value + "\n"
		}
	}

	return &discordgo.MessageEmbed{
		URL:         "https://imdb.com/title/" + m.ImdbID,
		Type:        "rich",
		Title:       m.Title,
		Description: m.Plot,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       settings.Color.Dec,
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Not the movie you were looking for?  Make sure you spelled it correctly and include any prepositions in the title.\n" +
				"Data provided by The Open Movie Database",
			IconURL: "https://m.media-amazon.com/images/G/01/IMDb/BG_square._CB1509067564_SY230_SX307_AL_.png",
		},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: m.Poster,
		},
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:   "Theater Release",
				Value:  m.ReleaseDate,
				Inline: true,
			},
			{
				Name:   "DVD Release",
				Value:  m.DvdRelease,
				Inline: true,
			},
			{
				Name:   "Length",
				Value:  m.Runtime,
				Inline: true,
			},
			{
				Name:   "Genre",
				Value:  m.Genre,
				Inline: true,
			},
			{
				Name:   "Director",
				Value:  m.Director,
				Inline: true,
			},
			{
				Name:   "Actors",
				Value:  m.Actors,
				Inline: true,
			},
			{
				Name:   "Language",
				Value:  m.Language,
				Inline: true,
			},
			{
				Name:   "Metascore",
				Value:  m.Metascore,
				Inline: true,
			},
			{
				Name:   "Awards",
				Value:  m.Awards,
				Inline: true,
			},
			{
				Name:   "Ratings",
				Value:  ratings,
				Inline: true,
			},
		},
	}
}
