/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package utilities

import log "github.com/sirupsen/logrus"

// Trace - Logs the event at the Trace level
func Trace(args ...interface{}) {
	if Settings.LogLevel == 0 {
		log.Traceln(args...)
	}
}

// Tracef - Logs the event at the Trace level with formatting
func Tracef(format string, args ...interface{}) {
	if Settings.LogLevel == 0 {
		log.Tracef(format, args...)
	}
}

// Debug - Logs the event at the Debug level
func Debug(args ...interface{}) {
	if Settings.LogLevel <= 1 {
		log.Debugln(args...)
	}
}

// Info - Logs the event at the Info level
func Info(args ...interface{}) {
	if Settings.LogLevel <= 2 {
		log.Infoln(args...)
	}
}

// Warning - Logs the event at the Warning level
func Warning(args ...interface{}) {
	if Settings.LogLevel <= 3 {
		log.Warningln(args...)
	}
}

// Error - Logs the event at the Error level
func Error(args ...interface{}) {
	if Settings.LogLevel <= 4 {
		log.Errorln(args...)
	}
}

// Fatal - Logs the event at the Fatal level
func Fatal(args ...interface{}) {
	if Settings.LogLevel <= 5 {
		log.Fatalln(args...)
	}
}
