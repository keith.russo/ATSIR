/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package utilities

import (
	"bufio"
	"compress/gzip"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/cheggaaa/pb/v3"
	"gitlab.com/aguemort/ATSIR/structs"
)

var (
	token    string
	settings structs.Settings
)

// DownloadGzFile - Downloads a g-zipped file and saves it to disk
func DownloadGzFile(path string, url string) {
	response, err := http.Get(url)
	if err != nil {
		Fatal("Error downloading city data for the Weather API...", err)
	}

	bar := pb.Simple.Start64(response.ContentLength)
	bar.SetRefreshRate(50 * time.Millisecond)

	defer response.Body.Close()

	outputFile, err := os.Create(path + ".gz")
	if err != nil {
		Fatal("Error creating the output file for weather cities", err)
	}
	defer outputFile.Close()

	barReader := bar.NewProxyReader(response.Body)

	_, err = io.Copy(outputFile, barReader)
	if err != nil {
		Fatal("Error occurred when writing gzipped content to the .gz file", err)
	}

	bar.Finish()
}

// GunzipFile - Unzips a g-zipped file and saves the decompressed data to disk
func GunzipFile(path string) {
	gunzip, err := os.Open(path + ".gz")
	if err != nil {
		Fatal("Error opening gunzipped file", err)
	}

	reader, err := gzip.NewReader(gunzip)
	if err != nil {
		Fatal("Error reading gzipped file", err)
	}
	defer reader.Close()

	writer, err := os.Create(path)
	if err != nil {
		Fatal("Error opening new file to put gunzipped contents into", err)
	}

	defer writer.Close()

	if _, err = io.Copy(writer, reader); err != nil {
		Fatal("Error Gunzipping file contents", err)
	}
}

// DeleteGzFile - Deletes the g-zipped file after decompressing it
func DeleteGzFile(path string) {
	// If the file is still in use (being gunzipped) it will error out, so we brute force try to delete it until it's
	// closed
	for {
		err := os.Remove(path + ".gz")
		if err != nil {
			time.Sleep(2 * time.Minute)
			continue
		}

		break
	}
}

// BotSettings - Sets default bot settings; partially interactive for self-hosted bots
func BotSettings() *structs.Settings {
	/*
		Check for and create directories and additional file structure if needed
	*/
	// data directory; holds json files that are external to the bot's core
	if _, err := os.Stat("data"); os.IsNotExist(err) {
		_ = os.Mkdir("data", os.ModeDir)
	}

	if _, err := os.Stat("logs"); os.IsNotExist(err) {
		_ = os.Mkdir("logs", os.ModeDir)
	}

	/*
		Check for the presence of bot.settings
		If it doesn't exist, create it and error out with directions
		If it does exist but not in the data directory, let's move it there
		TODO: encrypt settings file
	*/
	if _, err := os.Stat("data/bot.settings"); os.IsNotExist(err) {
		Info("Bot settings file not found.  Let's create it!")
		reader := bufio.NewReader(os.Stdin)

		fmt.Print("Discord Bot Token >> ")
		botToken, _ := reader.ReadString('\n')
		botToken = strings.Replace(botToken, "\r\n", "", -1) // for windows systems
		botToken = strings.Replace(botToken, "\n", "", -1)

		fmt.Print("Discord Bot Client ID >> ")
		clientID, _ := reader.ReadString('\n')
		clientID = strings.Replace(clientID, "\r\n", "", -1) // for Windows systems
		clientID = strings.Replace(clientID, "\n", "", -1)

		fmt.Print("What is the bot's command prefix? >>")
		prefix, _ := reader.ReadString('\n')
		prefix = strings.Replace(prefix, "\r\n", "", -1) // for Windows systems
		prefix = strings.Replace(prefix, "\n", "", -1)

		newSettings := structs.Settings{
			DiscordToken:    botToken,
			DiscordClientID: clientID,
			Language:        "en-us",
			Prefix:          prefix,
			IgnoreBots:      true,
			LogLevel:        structs.Info,
		}

		file, _ := json.MarshalIndent(newSettings, "", "  ")
		_ = ioutil.WriteFile("data/bot.settings", file, 0644)
	} else if _, err = os.Stat("bot.settings"); os.IsExist(err) {
		_ = os.Rename("bot.settings", "data/bot.settings")
	}

	/*
		Open and parse internal bot settings
	*/

	settingsRaw, err := ReadJSONFile("data/bot.settings")
	if err != nil {
		Fatal("Failed opening the bot file.  Exiting...", err)
		os.Exit(1)
	}

	err = json.Unmarshal(settingsRaw, &settings)
	if err != nil {
		Fatal("Error while unmarshalling the JSON.", err)
		os.Exit(1)
	}

	flag.StringVar(&token, "token", settings.DiscordToken, "Bot token")
	return &settings

}

// WeatherAPICityData - Fetches the cities JSON file for the Weather plugin
func WeatherAPICityData() {
	/*
		Check for and grab city list json for weather API
	*/
	if _, err := os.Stat("data/cities.weather"); os.IsNotExist(err) {
		fileLocation := "http://bulk.openweathermap.org/sample/city.list.min.json.gz"

		Info("Downloading city data for the weather plugin...  Please wait a moment...")
		DownloadGzFile("data/cities.weather", fileLocation)

		Info("Unzipping the file")
		GunzipFile("data/cities.weather")
	}
}

// ReadJSONFile - Returns a byte slice of raw info from a JSON file
func ReadJSONFile(fileName string) ([]byte, error) {
	file, err := os.Open(fileName)
	if err != nil {
		Fatal("Failed opening", fileName, "file.  Exiting...", err)
		return nil, err
	}

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		Fatal("Failed reading the", fileName, "file.  Exiting...", err)
		return nil, err
	}

	return fileBytes, nil
}
