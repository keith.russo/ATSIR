/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package utilities

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/aguemort/ATSIR/structs"
)

// SendPlainTextMessage - Sends a plain text message to Discord
func SendPlainTextMessage(ch *structs.Command, channelID string, message string) {
	_, err := ch.Session.ChannelMessageSend(channelID, message)
	if err != nil {
		Warning("Error sending Discord Message", err)
	}
}

// SendCodeBlockMessage - Sends a code block message to Discord
func SendCodeBlockMessage(ch *structs.Command, channelID string, message string, language string) bool {
	newMessage := "```" + language + "\n" + message + "\n```"
	if len(newMessage) > 2000 {
		return false
	}

	msg, err := ch.Session.ChannelMessageSend(channelID, newMessage)
	if err != nil {
		Warning("Error sending Discord Message", err)
	}
	Info("Message", msg.ID, "sent to Guild:", msg.GuildID)

	return true
}

// SendEmbedMessage - Sends a message containing an embed object to the channel the command was called from
func SendEmbedMessage(ch *structs.Command, message string, embed *discordgo.MessageEmbed) bool {
	msg, err := ch.Session.ChannelMessageSendEmbed(ch.Message.ChannelID, embed)
	if err != nil {
		Warning("Error sending the embed to Discord", err)
		return false
	}
	Info("Message", msg.ID, "sent to Guild:"+ch.Message.GuildID)

	_ = ch.Session.ChannelMessageDelete(ch.Message.ChannelID, ch.Message.ID)

	return true
}

// SendEmbedMessageElsewhere - Sends a message containing an embed object to a different channel in a guild
func SendEmbedMessageElsewhere(session *discordgo.Session, channelID string, embed *discordgo.MessageEmbed) *discordgo.Message {
	msg, err := session.ChannelMessageSendEmbed(channelID, embed)
	if err != nil {
		Warning("Error sending the embed to Discord", err)
		return nil
	}
	Info("Message", msg.ID, "sent to Guild:", msg.GuildID)

	return msg
}
